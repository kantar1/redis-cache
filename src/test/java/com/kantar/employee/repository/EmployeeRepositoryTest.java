package com.kantar.employee.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.kantar.employee.entity.Employee;

import jakarta.transaction.Transactional;

@DataJpaTest
@ExtendWith(SpringExtension.class)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)

@Transactional
public class EmployeeRepositoryTest {
	
	@Autowired
	private EmployeeRepository empRepository;
	
	private Employee employee1;
	private Employee employee2;
	private List<Employee> empList;
	
	@BeforeEach
	public void setup() {
		employee1 = new Employee(111,"emp1test","emp1test@gmail.com","+91-999999","development",33333.33);
		employee2 = new Employee(222,"emp2test","emp2test@gmail.com","+91-888888","testing",44444.44);
		this.empList = Arrays.asList(employee1,employee2);
	}
	
	@AfterEach
	public void delete() {
		employee1 = null;
		employee2 = null;
		this.empList = null;
		this.empRepository.deleteAll();
	}
	
	@Test
	public void should_save_an_employee() {
		Employee savedEmployee = this.empRepository.save(employee1);		
		assertThat(savedEmployee).isNotNull();
	    assertThat(savedEmployee.getId()).isNotNull();
	    assertThat(savedEmployee.getName()).isNotNull();
	}
	
	@Test
	public void should_give_an_employee_provided_id() {
			System.out.println(this.empRepository.save(employee1));		
			Employee employee = this.empRepository.findByEmail(employee1.getEmail()).get();
			System.out.println(employee);
			assertThat(employee).isNotNull();
			assertEquals("emp1test",employee.getName(),"names not matching");
	}
	

}
