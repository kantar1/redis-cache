package com.kantar.employee.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kantar.employee.entity.Employee;
import com.kantar.employee.exception.EmployeeAlreadyExistsException;
import com.kantar.employee.exception.EmployeeNotFoundException;
import com.kantar.employee.service.EmployeeServiceImpl;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {
	
	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private EmployeeServiceImpl empService;
	
	@InjectMocks
	private EmployeeController empController;
	
	private Employee employee1;
	private Employee employee2;
	private List<Employee> empList;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@BeforeEach
	public void setup() {
		
		employee1 = new Employee(111,"emp1test","emp1test@gmail.com","+91-999999","development",33333.33);
		employee2 = new Employee(222,"emp2test","emp2test@gmail.com","+91-888888","testing",44444.44);
		this.empList = Arrays.asList(employee1,employee2);
	}
	
	@AfterEach
	public void delete() {
		employee1 = null;
		employee2 = null;
		this.empList = null;
	}
	
	@Test
	public void should_return_all_employees() throws Exception {
		when(empService.getEmployees()).thenReturn(empList);
		mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/employees"))
		       .andExpect(MockMvcResultMatchers.status().isOk())
		       .andExpect(MockMvcResultMatchers.jsonPath("$.size()").value(empList.size()))
		       .andDo(print());
	}
	
	@Test
	public void should_return_employee_provided_id() throws Exception {
		when(empService.getEmployeeById(employee1.getId())).thenReturn(employee1);
		mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/employees/{id}",employee1.getId()))
	       .andExpect(MockMvcResultMatchers.status().isOk())
	       .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(employee1.getId()))
	       .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("emp1test"))
	       .andDo(print());
		
	}
	
	@Test
	public void should_throws_exception_provided_non_existing_employee() throws Exception{
		when(empService.getEmployeeById(employee1.getId())).thenThrow(new EmployeeNotFoundException());
		mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/employees/{id}",employee1.getId()))
		 .andExpect(MockMvcResultMatchers.status().isNotFound())
		 .andDo(print());        
	    
	}
	
	@Test
	@DisplayName("")
	public void should_save_employee() throws JsonProcessingException, Exception{
		when(empService.createEmployee(employee1)).thenReturn(employee1);
		mockMvc.perform(MockMvcRequestBuilders.post("/api/v1/employees")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(employee1)))  // Convert user object to JSON
            .andExpect(MockMvcResultMatchers.status().isCreated())
            .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
            .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("emp1test"));
		
	}

	
}
