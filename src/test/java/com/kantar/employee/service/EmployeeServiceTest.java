package com.kantar.employee.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.kantar.employee.entity.Employee;
import com.kantar.employee.exception.EmployeeAlreadyExistsException;
import com.kantar.employee.repository.EmployeeRepository;

@ExtendWith(MockitoExtension.class)
public class EmployeeServiceTest {
	
	@Mock
	private EmployeeRepository employeeRepository;
	
	@InjectMocks
	private EmployeeServiceImpl employeeService;
	
	private Employee employee1;
	private Employee employee2;
	private List<Employee> empList;
	
	
	
	@BeforeEach
	public void setup() {
		
		employee1 = new Employee(111,"emp1test","emp1test@gmail.com","+91-999999","development",33333.33);
		employee2 = new Employee(222,"emp2test","emp2test@gmail.com","+91-888888","testing",44444.44);
		this.empList = Arrays.asList(employee1,employee2);
	}
	
	@AfterEach
	public void delete() {
		employee1 = null;
		employee2 = null;
		this.empList = null;
	}
	
	@Test
	@DisplayName("should_create_employee")
	public void should_create_employee() throws EmployeeAlreadyExistsException {
		when(employeeRepository.findByEmail(employee1.getEmail())).thenReturn(Optional.empty());	
		when(employeeRepository.save(employee1)).thenReturn(employee1);
		Employee createdEmployee = employeeService.createEmployee(employee1);
		System.out.println(createdEmployee);
		assertThat(createdEmployee).isNotNull();
		assertEquals("emp1test", createdEmployee.getName());
		verify(employeeRepository, times(1)).save(employee1);
		
	}
	
	@Test
	@DisplayName("should_throw_exception_when_employee_exists_with_email")
	public void should_throw_exception_when_employee_exists_with_email() throws EmployeeAlreadyExistsException{
		when(employeeRepository.findByEmail(employee1.getEmail())).thenReturn(Optional.of(employee1));	
		assertThrows(EmployeeAlreadyExistsException.class, () -> employeeService.createEmployee(employee1));

		
	}
	
	
	
	

}
