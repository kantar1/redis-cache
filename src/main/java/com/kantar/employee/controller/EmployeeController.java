package com.kantar.employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kantar.employee.entity.Employee;
import com.kantar.employee.exception.EmployeeAlreadyExistsException;
import com.kantar.employee.exception.EmployeeNotFoundException;
import com.kantar.employee.service.EmployeeService;

@RestController
@RequestMapping("api/v1/employees")
public class EmployeeController {

	@Autowired
	EmployeeService empService;

	@GetMapping
	public ResponseEntity<?> getEmployees() {
		return ResponseEntity.ok(this.empService.getEmployees());
	}

	@PostMapping
	public ResponseEntity<?> createEmployee(@RequestBody Employee employee) throws EmployeeAlreadyExistsException {
		Employee createdEmployee;
		try {
			createdEmployee = this.empService.createEmployee(employee);
		} catch (EmployeeAlreadyExistsException e) {
			throw e;
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(createdEmployee);

	}	

	@GetMapping("/{id}")
	public ResponseEntity<?> getEmployee(@PathVariable int id) throws EmployeeNotFoundException{
		try {
			Employee employee = this.empService.getEmployeeById(id);
			return ResponseEntity.ok(employee);
		} catch (EmployeeNotFoundException e) {
			throw e;
		}		
	}

	@PutMapping
	public ResponseEntity<?> updateEmployee(@RequestBody Employee employee) throws EmployeeNotFoundException {
		try {
			return ResponseEntity.ok(this.empService.updateEmployee(employee));
		} catch (EmployeeNotFoundException e) {
			throw e;
		}
		
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable int id) throws EmployeeNotFoundException{
		try {
		return ResponseEntity.ok(this.empService.deleteEmployee(id));
		} catch(EmployeeNotFoundException e) {
			throw e;
		}
	}

}
