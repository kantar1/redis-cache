package com.kantar.employee.service;

import java.util.List;

import com.kantar.employee.entity.Employee;
import com.kantar.employee.exception.EmployeeAlreadyExistsException;
import com.kantar.employee.exception.EmployeeNotFoundException;

public interface EmployeeService {
	
	public Employee createEmployee(Employee employee) throws EmployeeAlreadyExistsException;
	public List<Employee> getEmployees();
	public Employee getEmployeeById(int id) throws EmployeeNotFoundException;
	public Employee updateEmployee(Employee employee) throws EmployeeNotFoundException;
	public boolean deleteEmployee(int id) throws EmployeeNotFoundException;

}
