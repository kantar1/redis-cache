package com.kantar.employee.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.kantar.employee.entity.Employee;
import com.kantar.employee.exception.EmployeeAlreadyExistsException;
import com.kantar.employee.exception.EmployeeNotFoundException;
import com.kantar.employee.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository empRepository;

	@Override
	public Employee createEmployee(Employee employee) throws EmployeeAlreadyExistsException {
		Optional<Employee> findByEmail = this.empRepository.findByEmail(employee.getEmail());
		if (findByEmail.isPresent())
			throw new EmployeeAlreadyExistsException();
		else
			return this.empRepository.save(employee);
	}

	@Override
	public List<Employee> getEmployees() {
		return this.empRepository.findAll();
	}

	@Override
	@Cacheable(key = "#id", value = "Employee", condition="#id != null")
	public Employee getEmployeeById(int id) throws EmployeeNotFoundException {
		Optional<Employee> findById = this.empRepository.findById(id);
		if(!findById.isPresent())
			throw new EmployeeNotFoundException();
		System.out.println("Getting from the database");
		return findById.get();
	}

	@Override
	@CachePut(key = "#id", value= "Employee",condition = "#id != null")
	public Employee updateEmployee(Employee employee) throws EmployeeNotFoundException{
		Optional<Employee> findById = this.empRepository.findById(employee.getId());
		if(!findById.isPresent())
			throw new EmployeeNotFoundException();
		System.out.println("Getting from the database");
		return this.empRepository.save(employee);
	}

	@Override
	@CacheEvict(key = "#id",value = "Employee")
	public boolean deleteEmployee(int id) throws EmployeeNotFoundException {
		Optional<Employee> findById = this.empRepository.findById(id);
		if(!findById.isPresent())
			throw new EmployeeNotFoundException();
		this.empRepository.delete(findById.get());
		return true;
	}

}
