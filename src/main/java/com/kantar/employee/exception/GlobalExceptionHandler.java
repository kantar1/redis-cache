package com.kantar.employee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler {
	
	@ExceptionHandler(EmployeeNotFoundException.class)
	public ResponseEntity<?> handleEmployeeNotFoundException(EmployeeNotFoundException e){
		return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Employee Not Found With this ID");
	}

	@ExceptionHandler(EmployeeAlreadyExistsException.class)
	public ResponseEntity<?> handleEmployeeAlreadyExistsException(EmployeeAlreadyExistsException e){
		return ResponseEntity.status(HttpStatus.CONFLICT).body("Employee Already Exists With this Email");
	}

	
}
